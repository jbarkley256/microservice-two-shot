# Wardrobify

Team:

* Jeremy Barkley - Shoes microservice
* Gavin Andrew - Hats microservice

## Design

## Shoes microservice

I made the Shoe model and BinVO model using the required attributes.
The BinVO polls the Wardrobe microservice to get Bin information
for the Shoe model.

## Hats microservice

I have a Hat model and a LocationVO model; the Hat model is
full of attributes regarding the hat in question, such as style fabric color etc.,
and then it has an attribute called location, a foreign key connection
to the other model LocationVO, which has attributes import_href and closet_name.

This LocationVO model is supplied information from a poller;
this poller makes a get request every so often to the wardrobe api
to get location data, and it takes the returned href and closet_name
and populates the LocationVO as import_href and closet_name.
