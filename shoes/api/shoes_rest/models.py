from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    shoe_model = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_bin", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("manufacturer", "shoe_model", "color", "picture")
