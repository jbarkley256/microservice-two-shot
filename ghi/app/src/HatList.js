import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const HatList = () => {
  const [ hats, setHats ] = useState([])

  useEffect(() => {
    const loadData = async () => {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
        console.log(data)
      } else {
        console.error("Failed to fetch");
      }
    }
    loadData()
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>hats</h1>
          <Link  to="/hats/new"><button className="btn btn-primary">add</button></Link>
          <table className="table">
            <thead>
              <tr>
                <th>fabric</th>
                <th>style</th>
                <th>color</th>
                <th>location</th>
                <th>picture</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                {hats?.map(hat =>
                  <tr key={hat.id}>
                    <td>{hat.fabric}</td>
                    <td>{hat.style}</td>
                    <td className="color" style={{ color: hat.color, backgroundColor: hat.color +'20' }}>{hat.color}</td>
                    <td>{hat.location.closet_name}</td>
                    <td><img src={hat.picture_url} alt='hat' style={{ width: 100 }}/></td>
                    <td><button style={{color: "#FF0000"}} className="btn" onClick={async () =>{
                      await fetch(`http://localhost:8090/api/hat/${hat.id}/`, {method:"DELETE"})
                      }}>remove</button>
                  </td></tr>)
                }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )

}

export default HatList;
