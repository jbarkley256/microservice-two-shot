import {useEffect, useState} from 'react';

const HatForm = () => {
  const [ formData, setFormData ] = useState({
    fabric: '',
    color: '',
    style: '',
    picture_url: '',
    location: ''
  })

  const [ locations, setLocations ] = useState([])

  useEffect(()=> {
    const loadData = async () => {
      const response = await fetch('http://localhost:8100/api/locations/');

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      } else {
        console.log("Failed to retrieve locations");
      }
    }

    loadData()
  }, [])

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData)
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch('http://localhost:8090/api/hats/', fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat)

      setFormData({
        fabric: '',
        color: '',
        style: '',
        picture_url: '',
        location: ''
      });
    }
  }

  return (
  <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New hat entry</h1>
          <form onSubmit={handleSubmit} id="enter-hat-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="..." required type="text" name="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="..." required type="color" name="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="..." required type="text" name="style"className="form-control" />
              <label htmlFor="style">Style</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="https://example.com" required type="url" name="picture_url"className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>{location.closet_name} ({location.section_number} sections {location.shelf_number} shelfs)</option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default HatForm;
