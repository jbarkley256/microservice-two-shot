import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const ShoeList = () => {
    const [ shoes, setShoes ] = useState([])

    useEffect(() => {
        const loadData = async () => {
            const response = await fetch('http://localhost:8080/api/shoes');
            if (response.ok) {
                const data = await response.json();
                setShoes(data.shoes);
                console.log(data)
            } else {
                console.error("Failed to fetch");
            }
        }
        loadData()
    }, [])

    return (
        <div>
            <div>
                <h1>Shoes</h1>
                <Link to="/shoes/new"><button>Add</button></Link>
                <table>
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Bin</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes?.map(shoe =>
                            <tr>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.shoe_model }</td>
                                <td>{ shoe.color }</td>
                                <td><img src={ shoe.picture } alt='shoe' style={{ width: 100 }} /></td>
                                <td>{ shoe.bin.closet_name }</td>
                                <td><button className ="btn" onClick={ async () =>{
                                    await fetch(`http://localhost:8080/api/shoes/${shoe.id}`, {method:"DELETE"})
                                }}>remove</button></td>
                            </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );

}


export default ShoeList;

/*

*/
