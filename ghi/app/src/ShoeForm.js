import { useEffect, useState } from "react";

const ShoeForm = () => {
    const [ formData, setFormData ] = useState({
        manufacturer: '',
        shoe_model: '',
        color: '',
        picture: '',
        bin: ''
    })

    const [ bins, setBins ] = useState([])

    useEffect(() => {
        const loadData = async () => {
            const response = await fetch('http://localhost:8100/api/bins/');

            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            } else {
                console.log("Failed to retrieve bins");
            }
        }

        loadData()
    }, [])

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log(formData)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch('http://localhost:8080/api/shoes/', fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            setFormData({
                manufacturer: '',
                shoe_model: '',
                color: '',
                picture: '',
                bin: ''
            });
        }
    }

    return <div className="row">
        <div>
            <div>
                <h1>New Shoe Entry</h1>
                <form onSubmit={ handleSubmit } id="enter-shoe-form">

                    <div>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <input onChange={ handleFormChange } placeholder="..." required type="text" name="manufacturer" className="form-control" />
                    </div>

                    <div>
                        <label htmlFor="shoe_model">Model</label>
                        <input onChange={ handleFormChange } placeholder="..." required type="text" name="shoe_model" className="form-control" />
                    </div>

                    <div>
                        <label htmlFor="color">Color</label>
                        <input onChange={ handleFormChange } placeholder="..." required type="text" name="color" className="form-control" />
                    </div>

                    <div>
                        <label htmlFor="picture">Picture URL</label>
                        <input onChange={ handleFormChange } placeholder="https://example.com" required type="url" name="picture" className="form-control" />
                    </div>

                    <br></br>
                    <div>
                        <select onChange={ handleFormChange } required name="bin" className="form-select">
                            <option value="">Choose a bin</option>
                            { bins.map(bin => {
                                return (
                                    <option key={ bin.id } value={ bin.id }>{ bin.closet_name }</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Add</button>
                </form>
            </div>
        </div>
    </div>
}

export default ShoeForm;
